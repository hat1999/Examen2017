#include "listaCompradores.h"
#include <iostream>

void inicializar(tListaComprador & lista){
  lista.compradores = new tComprador[TAM];
  lista.cont = 0;
  lista.tam = TAM;
}

void mostrar(tListaComprador lista){
  for (int i = 0; i < lista.cont; i++){
    cout << "Comprador: " << lista.compradores[i].comprador;
    cout << "Total: " << lista.compradores[i].importe;
  }
}

void liberar (tListaComprador & lista){
  delete[] lista.compradores;
  lista.compradores = nullptr;
  lista.cont = 0;
  lista.tam = 0;
}