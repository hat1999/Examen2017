#ifndef __LOTES__
#define __LOTES__

#include <cstddef>
#include <string>

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

typedef struct{
  string id;
  string tipo;
  float peso;
  float precio;
  long long int comprador;
} tLotes;

//PROTOTIPOS

void mostrarLote(tLotes lote);
void mostrarPrecio(tLotes lote);
void modificarLote(tLotes & lote, string id, float precio);

#endif