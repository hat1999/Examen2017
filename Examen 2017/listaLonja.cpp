#include "listaLonja.h"
#include <fstream>
#include <iostream>

int main(){
  tListaLonja lista_lonja;
  tLotes nuevo_lote;
  nuevo_lote.id = "KKSJDHISID";
  nuevo_lote.tipo = "Zurria";
  inicializar(lista_lonja);
  if(cargar(lista_lonja))
    mostrar(lista_lonja);
  cout << numLotes(lista_lonja);
  insertar(lista_lonja, nuevo_lote);
  mostrar(lista_lonja);
  return 0;
}

void inicializar(tListaLonja & lista_lonja){
  lista_lonja.num = 0;
}

bool cargar(tListaLonja & lista_lonja){
  bool cargado = false;
  int i = 0;
  ifstream archivo;
  archivo.open("lotes.txt");
  if (archivo.is_open()){
    while (!archivo.eof() && i < MAX){
      lista_lonja.lista[i] = new tLotes;
      archivo >> lista_lonja.lista[i] -> id;
      archivo >> lista_lonja.lista[i] -> tipo;
      archivo >> lista_lonja.lista[i] -> peso;
      archivo >> lista_lonja.lista[i] -> precio;
      lista_lonja.num++;
      i++;
    }
    archivo.close();
    cargado = true;
  }
  return cargado;
}

void mostrar(tListaLonja lista_lonja){
  for (int i = 0; i < lista_lonja.num; i++)
    mostrarLote(*lista_lonja.lista[i]);
}

int numLotes(tListaLonja lista_lonja){
  return lista_lonja.num;
}

void liberar(tListaLonja & lista_lonja){
  for (int i = 0; i < lista_lonja.num; i++)
    delete lista_lonja.lista[i];
}

tLotes obtenerLote(tListaLonja & lista_lonja, int posicion){
  tLotes seleccion;
  if (posicion >= 0 && posicion < lista_lonja.num)
    seleccion = *lista_lonja.lista[posicion];
  else
    cout << "ERROR 404: POSICION NOT FOUND";
  return seleccion;
}

bool insertar(tListaLonja & lista_lonja, tLotes nuevo_lote){
  bool insertado = false;
  bool existe = false;
  int pos;
  //existe = buscar(lista_lonja, nuevo_lote.id, nuevo_lote.tipo, 0, lista_lonja.num - 1, pos)
  if (!existe && lista_lonja.num < MAX){
    pos = 0;
    while (pos < lista_lonja.num && nuevo_lote.tipo > lista_lonja.lista[pos] -> tipo)
      pos++;
    while (pos < lista_lonja.num && lista_lonja.lista[pos] -> tipo == nuevo_lote.tipo && nuevo_lote.id > lista_lonja.lista[pos] -> id)
      pos++;
    if (pos < lista_lonja.num){
      for (int i = lista_lonja.num; pos < i; i--)
        lista_lonja.lista[i] = lista_lonja.lista[i - 1];
    }
    lista_lonja.lista[pos] = nullptr;
    lista_lonja.lista[pos] = new tLotes;
    lista_lonja.lista[pos] -> id = nuevo_lote.id;
    lista_lonja.lista[pos] -> tipo = nuevo_lote.tipo;
    lista_lonja.num++;
  }
  return insertado;
}

/*bool buscar(tListaLonja lista_lonja, string id, string tipo, int & ini, int & fin, int & pos){
  bool enc;
  if (ini <= fin){
    int mitad = (ini+fin)/2;
    if (tipo < lista_lonja.lista[mitad] -> tipo)
      buscar(lista_lonja, id, tipo, ini, mitad - 1, pos);
    else if (tipo > lista_lonja.lista[mitad] -> tipo)
      buscar(lista_lonja, id, tipo, mitad + 1, fin, pos);
    else{
      enc = true;
      pos = mitad;
    }
  }
  else {
    enc = false;
    pos = ini;
  }
  return enc;
}*/