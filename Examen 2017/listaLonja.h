#ifndef __LISTALONJA__
#define __LISTALONJA__

#include "lotes.h"
#include <cstddef>

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

const int MAX = 300;

typedef tLotes * tArrayPtr[MAX];

typedef struct{
  int num;
  tArrayPtr lista;
} tListaLonja;

//PROTOTIPOS

void inicializar(tListaLonja & lista_lonja);
bool cargar(tListaLonja & lista_lonja);
void mostrar(tListaLonja lista_lonja);
int numLotes(tListaLonja lista_lonja);
void liberar(tListaLonja & lista_lonja);
tLotes obtenerLote(tListaLonja & lista_lonja, int posicion);
bool buscar(tListaLonja lista_lonja, string id, string tipo, int & ini, int & fin, int & pos);
bool insertar(tListaLonja & lista_lonja, tLotes nuevo_lote);
#endif