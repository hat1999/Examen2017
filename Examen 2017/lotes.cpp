#include "lotes.h"
#include <iostream>
#include <iomanip>

void mostrarLote(tLotes lote){
  cout << "Lote: " << lote.id << setw(20) << "Peso del lote: " << lote.peso << endl;
  cout << "Tipo: " << lote.tipo << setw(20) << "Precio de salida: " << lote.precio << endl;
  cout << endl;
}

void mostrarPrecio(tLotes lote){
  cout << "Lote: " << lote.id << setw(20) << "Comprador: " << lote.comprador << endl;
  cout << "Tipo: " << lote.tipo << setw(20) << "Precio de compra: " << lote.precio << endl;
  cout << endl;
}

void modificarLote(tLotes & lote, string id, float precio){
  lote.id = id;
  lote.precio = precio;
}