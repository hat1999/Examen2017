#ifndef __LISTACOMPRADORES__
#define __LISTACOMPRADORES__

#include <cstddef>

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

const int TAM = 4;

typedef struct {
  long long int comprador;
  float importe;
} tComprador;

typedef struct{
  tComprador * compradores;
  int cont;
  int tam;
}tListaComprador;

void inicializar(tListaComprador & lista);
void liberar (tListaComprador & lista);
void mostrar(tListaComprador lista);

#endif