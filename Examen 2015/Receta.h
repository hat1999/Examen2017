#ifndef __RECETA__
#define __RECETA__

#include <cstddef>
#include <string>
#include "Componente.h"

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

const int TAM_RECETA = 20;

typedef tComponente * tPtrReceta;

typedef struct{
  tPtrReceta lista_componentes;
  string nombre;
  int capacidad;
  int cont;
}tReceta;

//PROTOTIPOS

void crea(tReceta & receta);
bool inserta (tReceta & receta, tComponente componente);
void muestra(tReceta receta);
void destruye_receta (tReceta & receta);

#endif