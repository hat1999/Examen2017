#ifndef __LISTAINGREDIENTES__
#define __LISTAINGREDIENTES__

#include <string>
#include <cstddef>

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

int const MAX = 50;

typedef struct{
  string nombre;
  int calorias;
} tIngrediente;

typedef tIngrediente * tArrayPtrIngrediente[MAX];

typedef struct{
  int cont;
  tArrayPtrIngrediente ingredientes;
} tListaIngredientes;

//PROTOTIPOS

bool carga(tListaIngredientes & lista);
string selecIngred(tListaIngredientes lista);
bool busca(const tListaIngredientes & lista, string buscado, int ini, int fin, int & pos);
bool inserta_ing (tListaIngredientes & lista, tIngrediente nuevo_ingrediente);

#endif