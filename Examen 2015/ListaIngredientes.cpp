#include "ListaIngredientes.h"
#include <fstream>
#include <iostream>
#include <stdio.h>

bool inserta_ing (tListaIngredientes & lista, tIngrediente nuevo_ingrediente){
  bool insertado = false;
  bool enc = false;
  int pos;
  cout << "hola";
  getchar();
  if (lista.cont < MAX){
      enc = busca (lista, nuevo_ingrediente.nombre, 0, lista.cont, pos);
      if (!enc && pos == -1){
        while (pos < lista.cont - 1 && !enc){
          if (nuevo_ingrediente.nombre > lista.ingredientes[pos] -> nombre)
            pos++;
          else
            enc = true;
        }
        for (int i = lista.cont - 1; pos < i; i--)
          lista.ingredientes[i + 1] = lista.ingredientes[i];
        lista.ingredientes[pos] = nullptr;
        lista.ingredientes[pos] = new tIngrediente;
        lista.ingredientes[pos] -> nombre = nuevo_ingrediente.nombre;
        lista.ingredientes[pos] -> calorias = nuevo_ingrediente.calorias;
        lista.cont++;
        insertado = true;
      }
  }
  return insertado;
}

bool carga(tListaIngredientes & lista){
  bool cargado = false;
  ifstream archivo;
  int lect;
  string calorias;
  archivo.open("ingredientes.txt");
  if (archivo.is_open()){
    archivo >> lect;
    while (lista.cont < MAX && lect != -1){
      lista.ingredientes[lista.cont] = new tIngrediente;
      lista.ingredientes[lista.cont] -> calorias = lect;
      archivo >> calorias;
      lista.ingredientes[lista.cont] -> nombre = calorias;
      lista.cont++;
      archivo >> lect;
    }
    archivo.close();
    cargado = true;
  }
  return cargado;
};

string selecIngred(tListaIngredientes lista){
  int num;
  for (int i = 0; i < lista.cont; i++)
    cout << i + 1 << ". " << lista.ingredientes[i] -> nombre << endl;
  do{
    cout << "Inserta el ingrediente por teclado: ";
    cin >> num;
  } while (num < 0 || num > lista.cont);
  return lista.ingredientes[num - 1] -> nombre;
}

bool busca(const tListaIngredientes & lista, string buscado, int ini, int fin, int & pos){
  bool enc;
  int mitad;
  if (ini <= fin){
    mitad = (ini + fin)/2;
    if (buscado < lista.ingredientes[mitad] -> nombre)
      enc = busca (lista, buscado, ini, mitad - 1, pos);
    else if (buscado > lista.ingredientes[mitad] -> nombre)
      enc = busca (lista, buscado, mitad + 1, fin, pos);
    else{
      enc = true;
      pos = mitad;
    }
  }
  else {
    enc = false;
    pos = -1;
  }
  return enc;
}