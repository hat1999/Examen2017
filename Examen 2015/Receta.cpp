#include "Receta.h"
#include <iostream>
#include <stdio.h>

void crea(tReceta & receta){
  receta.lista_componentes = new tComponente[TAM_RECETA];
  receta.capacidad = TAM_RECETA;
  receta.cont = 0;
}

bool inserta (tReceta & receta, tComponente componente){
  bool insertado = false;
  if (receta.cont < receta.capacidad){
    receta.lista_componentes[receta.cont] = componente;
    receta.cont++;
    insertado = true;
  }
  return insertado;
}

void muestra(tReceta receta){
  int suma = 0;
  cout << "Receta: " << receta.nombre << endl;
  for (int i = 0; i < receta.cont; i++){
    muestra(receta.lista_componentes[i]);
    suma = ((receta.lista_componentes[i].ptr_ingrediente -> calorias * receta.lista_componentes[i].gramos) / 100) + suma;
  }
  cout << "Total de calorias = " << suma;
}

void destruye_receta (tReceta & receta){
  delete[] receta.lista_componentes;
  receta.lista_componentes = nullptr;
}