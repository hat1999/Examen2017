#include "Componente.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <stdio.h>

tComponente leeComponente (tListaIngredientes lista){
  tComponente componente;
  string seleccion;
  bool enc;
  int pos;
  do{
  seleccion = selecIngred(lista);
  enc = busca(lista, seleccion, 0, lista.cont - 1, pos);
  } while(!enc);
  componente.ptr_ingrediente = lista.ingredientes[pos];
  cout << "Introduce cantidad en gramos: ";
  cin >> componente.gramos;
  return componente;
}

void muestra(tComponente componente){
  cout << setw(20) << componente.gramos << " gr. de " << componente.ptr_ingrediente -> nombre << endl;
}

void destuye_componente(tComponente & componente){
  delete componente.ptr_ingrediente;
  componente.ptr_ingrediente = nullptr;
}