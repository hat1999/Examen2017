#ifndef __COMPONENTE__
#define __COMPONENTE__

#include <cstddef>
#include "ListaIngredientes.h"

#if !defined(nullptr)
#define nullptr NULL
#endif

using namespace std;

typedef tIngrediente * tPtrIngrediente;

typedef struct{
  tPtrIngrediente ptr_ingrediente;
  int gramos;
} tComponente;

//PROTOTIPOS

tComponente leeComponente (tListaIngredientes lista);
void muestra(tComponente componente);
void destuye_componente(tComponente & componente);


#endif