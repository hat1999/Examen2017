#include "ListaIngredientes.h"
#include "Receta.h"
#include <iostream>
#include <stdio.h>
int menu();

int main(){
  int seleccion;
  tListaIngredientes lista;
  tReceta receta;
  tComponente componente;
  tIngrediente jamon;
  if (carga(lista)){
    crea(receta);
    do{
      seleccion = menu();
      if (seleccion == 1){
        componente = leeComponente(lista);
        if (!inserta(receta, componente))
          cout << "No se ha podido insertar";
      }
      else if (seleccion == 2){
        muestra(receta);
      }
    } while(seleccion != 0);
  }
  return 0;
}

int menu(){
  int seleccion;
  do{
    cout << "1. Anadir ingrediente" << endl;
    cout << "2. Mostrar estado de receta" << endl;
    cout << "0. Salir" << endl;
    cout << "Introduce una opcion";
    cin >> seleccion;
  } while (seleccion < 0 || seleccion > 2);
  return seleccion;
}